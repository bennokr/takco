takco.reshape package
=====================

.. automodule:: takco.reshape
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   takco.reshape.compound
   takco.reshape.findpivot
   takco.reshape.headers
