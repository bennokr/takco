takco.extract package
=====================

.. automodule:: takco.extract
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   takco.extract.clean
   takco.extract.database
   takco.extract.fetahu
   takco.extract.htmltables
