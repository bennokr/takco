takco.cluster package
=====================

.. automodule:: takco.cluster
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   takco.cluster.matchers

Submodules
----------

.. toctree::
   :maxdepth: 4

   takco.cluster.clustering
   takco.cluster.context
   takco.cluster.headerunions
