takco
=====

:mod:`tacko` is a modular system for extracting knowledge from tables. For example, you can use it
to extend `Wikidata <http://wikidata.org>`_ with information from Wikipedia tables.


Getting Started
~~~~~~~~~~~~~~~

Download the sources and build:

.. code-block:: shell

    pip install .

To view the command line help, run ``takco -h``.

Example run:


.. code-block:: shell

    tacko run -C config.toml your-pipeline.toml

