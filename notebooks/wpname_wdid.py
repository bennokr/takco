import sqlite3
import tqdm
import trident

kbgraph = trident.Db('kb/wikidata/wikidata2019-P31inf-wikibase-simple-qualifiers/')
p_wikiPageID = kbgraph.lookup_id("<wikipediaId>")

with sqlite3.connect('data_old/wpname_id.sqlite') as con:
    total = con.execute('select max(rowid) from wpname_id').fetchone()[0]
    
    for title, i in tqdm.tqdm(con.execute('select * from wpname_id'), total=total):
        n = kbgraph.lookup_id(f'"{i}"')
        if n is not None:
            for s in kbgraph.s(p_wikiPageID, n):
                print(kbgraph.lookup_str(s), title)